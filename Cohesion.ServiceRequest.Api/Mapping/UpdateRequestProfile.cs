﻿using AutoMapper;
using Cohesion.ServiceRequest.Api.Core.Models;
using Cohesion.ServiceRequest.Api.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cohesion.ServiceRequest.Api.Mapping
{
    public class UpdateRequestProfile : Profile
    {
        public UpdateRequestProfile()
        {
            CreateMap<UpdateServiceRequest, DbServiceRequest>();
        }
    }
}
