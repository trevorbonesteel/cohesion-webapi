﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cohesion.ServiceRequest.Api.Core;
using Cohesion.ServiceRequest.Api.Core.Models;
using Cohesion.ServiceRequest.Api.Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cohesion.ServiceRequest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceRequestController : ControllerBase
    {
        private readonly IServiceRequestService _serviceRequestService;
        private readonly IMapper _mapper;

        public ServiceRequestController(IServiceRequestService serviceRequestService, IMapper mapper)
        {
            this._serviceRequestService = serviceRequestService;
            this._mapper = mapper;
        }

        // GET: api/ServiceRequest
        [HttpGet]
        public async Task<IActionResult> GetAllServiceRequests()
        {
            var serviceRequests = await _serviceRequestService.ListAsync();
            var resources = _mapper.Map<IEnumerable<DbServiceRequest>, IEnumerable<ServiceRequestResource>>(serviceRequests);
            
            if (resources?.Count() != 0)
                return Ok(resources);
            else
                return NoContent();
        }
        // GET: api/ServiceRequest/3
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRequestById(Guid id)
        {
            var serviceRequests = await _serviceRequestService.FindAsync(id);
            var resources = _mapper.Map<DbServiceRequest, ServiceRequestResource>(serviceRequests);

            if (resources != null)
                return Ok(resources);
            else
                return NotFound();
        }

        // POST: api/ServiceRequest
        [HttpPost]
        public async Task<IActionResult> CreateServiceRequest([FromBody] CreateServiceRequest serviceRequest)
        {
            //check model state
            if (!ModelState.IsValid)
                return BadRequest();

            //map to db entity
            var dbServiceRequest = _mapper.Map<CreateServiceRequest, DbServiceRequest>(serviceRequest);

            //try call save
            try
            {
                await _serviceRequestService.SaveAsync(dbServiceRequest);
            }
            catch (Exception e)
            {
                //log exception here
                Console.WriteLine(e);
                return BadRequest();
            }


            var serviceRequestResource = _mapper.Map<DbServiceRequest, ServiceRequestResource>(dbServiceRequest);

            var result = new CreatedAtActionResult("createservicerequest", "servicerequest", "", serviceRequestResource);

            return result;

        }

        // PUT: api/ServiceRequest/3
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateServiceRequest(Guid id, [FromBody] UpdateServiceRequest serviceRequest)
        {//check model state
            if (!ModelState.IsValid)
                return BadRequest(new { message = "bad service request" });

            //map to db entity
            var dbServiceRequest = _mapper.Map<UpdateServiceRequest, DbServiceRequest>(serviceRequest);
            //try call save
            try
            {
                var updateResult = await _serviceRequestService.UpdateAsync(id, dbServiceRequest);
                if (updateResult == UpdateServiceResponse.NotFound)
                    return NotFound();
            }
            catch (Exception e)
            {
                //log exception here
                Console.WriteLine(e);
                return BadRequest(new { message = "bad service request" });
            }


            var serviceRequestResource = _mapper.Map<DbServiceRequest, ServiceRequestResource>(dbServiceRequest);

            var result = new CreatedAtActionResult("createservicerequest", "servicerequest", "", new { message = "created service request with id: " + serviceRequestResource.Id });

            return Ok(new { message = "updated service request" });
        }

        // DELETE: api/ServiceRequest/3
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _serviceRequestService.DeleteAsync(id);
            if (result)
                return Ok();
            else
                return NotFound();
        }
    }
}