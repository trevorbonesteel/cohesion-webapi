README

VisualStudio 2019 solution

Frameworks/Packages Used:
Microsoft.AspNetCore.App    v3.0.0
Microsoft.NETCore.App       v3.0.0
EntityFrameworkCore         v3.1.1
AutoMapper                  v9.0.0
AutoMapper.Extensions.Microsoft.DependencyInjection v7.0.0

In Memory Database seeded with the following data for testing:
{
    Id = 727b376b-79ae-498e-9cff-a9f51b848ea4,
    BuildingCode = "COH",
    Description = "Please turn up the AC in suite 1200D.It is too hot here.",
    CurrentStatus = Created,
    CreatedBy = "Nik Patel",
    CreatedDate = 2019-08-01T14:25:43.511Z,
    LastModifiedBy = "Jane Doe",
    LastModifiedDate = 2019-08-01T15:01:23.511Z
}


Testing Plan

Postman used to test throughout development.
Test Collection can be found at the following link
(https://app.getpostman.com/run-collection/bb6ec9fae69c53d611e2)


Enhancements to come:
- Authentication
- More robust logging
- Extend model validation error responses


Web API EndPoints

GET
    - Description: Read all Service requests
    - Route: https://localhost:5001/api/servicerequest
    - Response
        - 200: list of service requests
        - 204: empty content

GET
    - Decription: Read service request by id
    - Route: https://localhost:5001/api/servicerequest/{id}
    - Response
        - 200: single service request
        - 404: Not Found

POST
    - Decription: Create new service request
    - Route: https://localhost:5001/api/servicerequest
    - Response
        - 201: created service request with id
        - 400: bad request

PUT
    - Decription: update service request based on id
    - Route: https://localhost:5001/api/servicerequest/{id}
    - Response
        - 200: updated service request
        - 400: bad service request
        - 404: not found

DELETE
    - Decription: delete service request based on id
    - Route: https://localhost:5001/api/servicerequest/{id}
    - Response
        - 201: successful
        - 404: not found