﻿using Microsoft.EntityFrameworkCore;
using Cohesion.ServiceRequest.Api.Data.Entities;
using System;

namespace Cohesion.ServiceRequest.Api.Data
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        {
        }

        public DbSet<DbServiceRequest> DbServiceRequests { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<DbServiceRequest>().HasData
            (
                new DbServiceRequest
                {
                    Id = new Guid("727b376b-79ae-498e-9cff-a9f51b848ea4"),
                    BuildingCode = "COH",
                    Description = "Please turn up the AC in suite 1200D.It is too hot here.",
                    CurrentStatus = ServiceRequestStatus.Created,
                    CreatedBy = "Nik Patel",
                    CreatedDate = DateTime.Parse("2019-08-01T14:25:43.511Z"),
                    LastModifiedBy = "Jane Doe",
                    LastModifiedDate = DateTime.Parse("2019-08-01T15:01:23.511Z")
                }
            );
        }

    }
}
