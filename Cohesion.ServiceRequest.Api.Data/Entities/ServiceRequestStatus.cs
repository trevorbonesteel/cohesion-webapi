﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cohesion.ServiceRequest.Api.Data.Entities
{
    public enum ServiceRequestStatus : int
    {
        [Description("NotApplicable")]
        NotApplicable = 0,

        [Description("Created")]
        Created = 1,

        [Description("InProgress")]
        InProgress = 2,

        [Description("Complete")]
        Complete = 3,

        [Description("Cancelled")]
        Canceled = 4
    }
}
