﻿using Cohesion.ServiceRequest.Api.Core.Models;
using Cohesion.ServiceRequest.Api.Data;
using Cohesion.ServiceRequest.Api.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cohesion.ServiceRequest.Api.Core
{
    public class ServiceRequestService : IServiceRequestService
    {
        private readonly ApiDbContext _dbContext;

        public ServiceRequestService(ApiDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<IEnumerable<DbServiceRequest>> ListAsync()
        {
            return await _dbContext.DbServiceRequests.ToListAsync();
        }

        public async Task<DbServiceRequest> FindAsync(Guid id)
        {
            return await _dbContext.DbServiceRequests.FindAsync(id);
        }

        public async Task SaveAsync(DbServiceRequest serviceRequest)
        {
            serviceRequest.CreatedDate = DateTime.Now;

            await _dbContext.AddAsync(serviceRequest);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<UpdateServiceResponse> UpdateAsync(Guid id, DbServiceRequest serviceRequest)
        {
            var existingRequest = await _dbContext.DbServiceRequests.FindAsync(id);
            if (existingRequest == null)
                return UpdateServiceResponse.NotFound;

            existingRequest.BuildingCode = serviceRequest.BuildingCode;
            existingRequest.CurrentStatus = serviceRequest.CurrentStatus;
            existingRequest.Description = serviceRequest.Description;
            existingRequest.LastModifiedBy = serviceRequest.LastModifiedBy;
            existingRequest.LastModifiedDate = DateTime.Now;

            try
            {
                _dbContext.Update(existingRequest);
                await _dbContext.SaveChangesAsync();

                return UpdateServiceResponse.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return UpdateServiceResponse.BadServiceRequest;
            }
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var request = await _dbContext.DbServiceRequests.FindAsync(id);
            if (request == null)
                return false;
            try
            {
                _dbContext.Remove(request);
                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}
