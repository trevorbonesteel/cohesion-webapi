﻿using Cohesion.ServiceRequest.Api.Core.Models;
using Cohesion.ServiceRequest.Api.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cohesion.ServiceRequest.Api.Core
{
    public interface IServiceRequestService
    {
        Task<IEnumerable<DbServiceRequest>> ListAsync();
        Task<DbServiceRequest> FindAsync(Guid id);
        Task SaveAsync(DbServiceRequest saveServiceRequest);
        Task<UpdateServiceResponse> UpdateAsync(Guid id, DbServiceRequest serviceRequest);
        Task<bool> DeleteAsync(Guid id);
    }
}
