﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cohesion.ServiceRequest.Api.Core.Models
{
    public class ServiceRequestResource
    {
        public Guid Id { get; set; }
        public string BuildingCode { get; set; }
        public string Description { get; set; }
        public int CurrentStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
