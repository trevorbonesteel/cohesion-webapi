﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Cohesion.ServiceRequest.Api.Core.Models
{
    public class CreateServiceRequest
    {
        [Required]
        public string BuildingCode { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int CurrentStatus { get; set; }
        [Required]
        [MaxLength(50)]
        public string CreatedBy { get; set; }
    }
}
