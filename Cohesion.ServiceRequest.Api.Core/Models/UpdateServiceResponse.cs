﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cohesion.ServiceRequest.Api.Core.Models
{
    public enum UpdateServiceResponse : int
    {
        [Description("Success")]
        Success = 0,

        [Description("BadServiceRequest")]
        BadServiceRequest = 1,

        [Description("NotFound")]
        NotFound = 2,
    }
}
